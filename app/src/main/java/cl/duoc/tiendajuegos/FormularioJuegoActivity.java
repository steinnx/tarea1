package cl.duoc.tiendajuegos;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Calendar;

import cl.duoc.tiendajuegos.bd.ColeccionPelicula;
import cl.duoc.tiendajuegos.bd.ColeccionUsuario;
import cl.duoc.tiendajuegos.entidades.Pelicula;

public class FormularioJuegoActivity extends AppCompatActivity {

    private EditText edtTitulo, edtUrlI, edtDirector;
    private Button btnFecha;
    private Spinner cmbCate, cmbOpCalifi;
    private ImageView ivPreview;
    private Button btnVerImagen, btnVolver, btnRegistrarPelicula, btnListar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_juego);
        //Lleno los Spinner
        llenarCategoria();
        llenarCalificacion();
        //Recupero datos del XML
        edtTitulo = (EditText) findViewById(R.id.edtTitulo);
        edtUrlI = (EditText) findViewById(R.id.etUrlImagen);
        edtDirector = (EditText) findViewById(R.id.edtDirector);

        ivPreview = (ImageView) findViewById(R.id.ivPreview);

        cmbCate = (Spinner) findViewById(R.id.cbmOpCategorias);
        cmbOpCalifi = (Spinner) findViewById(R.id.cbmCalifi);

        btnVerImagen = (Button) findViewById(R.id.btnVerImagen);
        btnFecha = (Button) findViewById(R.id.btnFechEstr);
        btnRegistrarPelicula = (Button) findViewById(R.id.btnRegistrataPelicula);
        btnVolver = (Button) findViewById(R.id.btnVolverFormJuego);
        btnListar=(Button) findViewById(R.id.btnListar);


        //Click Botones

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listarPelicula();
            }
        });
        //Ingresar Una Pelicula a la ColeccionPelicula
        btnRegistrarPelicula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresarPelicula();
            }
        });
        //volver
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volver();
            }
        });
        //Carga Imagen
        btnVerImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarImagen(edtUrlI.getText().toString());
            }
        });
        //Desplegar DatePicker y Recuperar fecha
        btnFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });

    }

    private void listarPelicula() {
        Intent i = new Intent(FormularioJuegoActivity.this, ListarCustomizadaActivity.class);
        startActivity(i);
    }

    private void ingresarPelicula() {
        String mensajeError = "";
        if (edtTitulo.getText().toString().length() < 0) {
            mensajeError += " Error Titulo \n";
        }
        if (edtDirector.getText().toString().length() < 0) {
            mensajeError += " Error Director \n";
        }
        if (edtUrlI.getText().toString().length() < 0) {
            mensajeError += " Error Url \n";
        }
        if (btnFecha.getText().toString().length() < 0) {
            mensajeError += " Error Fecha \n";
        } else {
            Pelicula pel = new Pelicula();
            pel.setTitulo(edtTitulo.getText().toString());
            pel.setDirector(edtDirector.getText().toString());
            pel.setUrl(edtUrlI.getText().toString());
            pel.setFechaEstreno(btnFecha.getText().toString());
            pel.setCalificacion(cmbOpCalifi.getItemAtPosition(cmbOpCalifi.getSelectedItemPosition()).toString());
            pel.setCategoria(cmbCate.getItemAtPosition(cmbCate.getSelectedItemPosition()).toString());
            ColeccionPelicula.agregarPelicula(pel);
            Toast.makeText(this, "Pelicula Registrada", Toast.LENGTH_SHORT).show();
        }
        Toast.makeText(this, mensajeError, Toast.LENGTH_SHORT).show();
    }

    private void volver() {
        Intent i = new Intent(FormularioJuegoActivity.this, LoginActivity.class);
        startActivity(i);
    }

    private void cargarImagen(String url) {
        Picasso.with(this).load(url).into(ivPreview);
    }

    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                btnFecha.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }


    private void llenarCalificacion() {
        final String[] datos =
                new String[]{"+0", "+11", "+15", "+18"};

        ArrayAdapter<String> adaptador =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, datos);
        cmbOpCalifi = (Spinner) findViewById(R.id.cbmCalifi);

        adaptador.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);

        cmbOpCalifi.setAdapter(adaptador);
    }

    private void llenarCategoria() {

        final String[] datos =
                new String[]{"Terror", "Suspenso", "Fantasia", "Drama", "Comedia"};

        ArrayAdapter<String> adaptador =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, datos);
        cmbCate = (Spinner) findViewById(R.id.cbmOpCategorias);

        adaptador.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);

        cmbCate.setAdapter(adaptador);
    }
}
