package cl.duoc.tiendajuegos.bd;

import java.util.ArrayList;

import cl.duoc.tiendajuegos.entidades.Pelicula;

/**
 * Created by Jorge on 07-04-2017.
 */

public class ColeccionPelicula {
    private static ArrayList<Pelicula> Peliculas = new ArrayList<>();

    public static void agregarPelicula(Pelicula peli){
        Peliculas.add(peli);
    }

    public static ArrayList<Pelicula> getPelicula(){
        return Peliculas;
    }
}
