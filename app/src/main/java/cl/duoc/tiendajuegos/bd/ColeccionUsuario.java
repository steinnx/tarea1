package cl.duoc.tiendajuegos.bd;

import java.util.ArrayList;

import cl.duoc.tiendajuegos.entidades.Usuario;

/**
 * Created by Jorge on 07-04-2017.
 */

public class ColeccionUsuario {
    private static ArrayList<Usuario> usuarios = new ArrayList<>();

    public static void agregarFormulario(Usuario usu){
        usuarios.add(usu);
    }

    public static ArrayList<Usuario> getUsuarios(){
        return usuarios;
    }

}
