package cl.duoc.tiendajuegos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;

import cl.duoc.tiendajuegos.entidades.Pelicula;

/**
 * Created by DUOC on 06-04-2017.
 */

public class PeliculaAdapter extends ArrayAdapter<Pelicula> {

    private ArrayList<Pelicula> dataSource;

    public PeliculaAdapter(Context context, ArrayList<Pelicula> dataSource){
        super(context,R.layout.item_juego,dataSource);
        this.dataSource=dataSource;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.item_juego, null);


        TextView tveTitulo=(TextView)  item.findViewById(R.id.tvTitulo);
        tveTitulo.setText(dataSource.get(position).getTitulo());

        TextView tveDirector=(TextView)  item.findViewById(R.id.tvDirector);
        tveDirector.setText(dataSource.get(position).getDirector());

        TextView tveFechaEstreno=(TextView)  item.findViewById(R.id.tvFechaEstreno);
        tveFechaEstreno.setText(dataSource.get(position).getFechaEstreno());

        TextView tveCategoria=(TextView)  item.findViewById(R.id.tvCategoria);
        tveCategoria.setText(dataSource.get(position).getCategoria());

        TextView tveCalificacion=(TextView)  item.findViewById(R.id.tvCalificacion);
        tveCalificacion.setText(dataSource.get(position).getCalificacion());

        ImageView ivePreview = (ImageView)item.findViewById(R.id.ivPreview);
        Picasso.with(getContext())
                .load(dataSource.get(position).getUrl())
                .into(ivePreview);


        return(item);
    }

}
