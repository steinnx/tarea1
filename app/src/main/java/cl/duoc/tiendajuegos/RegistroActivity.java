package cl.duoc.tiendajuegos;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import cl.duoc.tiendajuegos.bd.ColeccionUsuario;
import cl.duoc.tiendajuegos.entidades.Usuario;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener  {
    private Button btnRegistrarse, btnVolver, etFecha;
    private EditText etUsu, etNom, etApe, etRut, etCon1, etCon2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        etUsu = (EditText) findViewById(R.id.editTextUsuario);
        etNom = (EditText) findViewById(R.id.editTextNombre);
        etApe = (EditText) findViewById(R.id.editTextApellido);
        etRut = (EditText) findViewById(R.id.editTextRut);
        etFecha = (Button) findViewById(R.id.btnFechNacimiento);
        etCon1 = (EditText) findViewById(R.id.editTextClave1);
        etCon2 = (EditText) findViewById(R.id.editTextClave2);

        etFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });
        btnRegistrarse = (Button) findViewById(R.id.btnRegistrarse);
        btnVolver = (Button) findViewById(R.id.btnVolver);

        btnRegistrarse.setOnClickListener(this);
        btnVolver.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnRegistrarse) {
            String mensajeError = "";
            if (validarRut(etRut.getText().toString())){

                if (etUsu.getText().toString().length()<0){
                    mensajeError+=" Error Usuario \n";
                }
                if (etNom.getText().toString().length()<0){
                    mensajeError+=" Nombre \n";
                }
                if (etApe.getText().toString().length()<0){
                    mensajeError+=" Apellido \n";
                }
                if (!etCon1.getText().toString().equals(etCon2.getText().toString())) {
                    mensajeError += " Contraseña Incorrecta \n";
                }
                else{
                    Usuario form = new Usuario();
                    form.setUsuario(etUsu.getText().toString());
                    form.setNombre(etNom.getText().toString());
                    form.setApellido(etApe.getText().toString());
                    form.setRut(etRut.getText().toString());
                    form.setFechaNacimiento(etFecha.getText().toString());
                    form.setContraseña(etCon1.getText().toString());

                    ColeccionUsuario.agregarFormulario(form);
                    Toast.makeText(this, "Usuario Registrado", Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(this, mensajeError, Toast.LENGTH_SHORT).show();


            }else{
                Toast.makeText(this, "Rut Incorrecto", Toast.LENGTH_SHORT).show();
            }
        } else if (v.getId() == R.id.btnVolver) {
            Intent i = new Intent(RegistroActivity.this,LoginActivity.class);
            startActivity(i);
        }

    }

    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etFecha.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }
    public static boolean validarRut(String rut) {

        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }
}
