package cl.duoc.tiendajuegos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.tiendajuegos.bd.ColeccionPelicula;
import cl.duoc.tiendajuegos.bd.ColeccionUsuario;
import cl.duoc.tiendajuegos.entidades.Usuario;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edtUsuario, edtContraseña;
    private Button btnEntrar, btnRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtUsuario = (EditText) findViewById(R.id.editUsuario);
        edtContraseña = (EditText) findViewById(R.id.editContraseña);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnRegistro = (Button) findViewById(R.id.btnRegistro);

        btnEntrar.setOnClickListener(this);
        btnRegistro.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnEntrar) {
            for (Usuario user : ColeccionUsuario.getUsuarios()) {
                // Toast.makeText(LoginActivity.this,user.getUsuario().toString()+user.getContraseña().toString(), Toast.LENGTH_SHORT).show();
                if (user.getUsuario().equals(edtUsuario.getText().toString())
                        && user.getContraseña().equals(edtContraseña.getText().toString()))
                {
                    Toast.makeText(LoginActivity.this, "Usuario Valido", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(LoginActivity.this, FormularioJuegoActivity.class);
                    startActivity(i);
                }
            }
            Toast.makeText(LoginActivity.this, "Usuario inValido", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.btnRegistro) {
            Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
            startActivity(i);
        }
    }
}
