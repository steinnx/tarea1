package cl.duoc.tiendajuegos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import cl.duoc.tiendajuegos.bd.ColeccionPelicula;
import cl.duoc.tiendajuegos.entidades.Pelicula;

public class ListarCustomizadaActivity extends AppCompatActivity {

    private ListView lbView;
    private ArrayList<Pelicula> dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_customizada);
        lbView =(ListView) findViewById(R.id.lvPeliculasCustom) ;

        PeliculaAdapter adaptador = new PeliculaAdapter(this, getDataSource());
        lbView.setAdapter(adaptador);

        /*lbView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListarCustomizadaActivity.this,
                        getDataSource().get(position).getTitulo(), Toast.LENGTH_LONG).show();
            }
        });*/

    }
    private ArrayList<Pelicula> getDataSource(){

        /*
        if(dataSource == null) {
            dataSource = new ArrayList<>();

            for (int x = 0; x < 50; x++) {
                Pelicula aux = new Pelicula();
                aux.setTitulo("Globo Bombastico");
                aux.setDirector("SuperCell");
                aux.setUrl("http://vignette3.wikia.nocookie.net/clash-royale-esp/images/d/d6/Globo_Bombardero.png/revision/latest?cb=20160307223203&path-prefix=es");
                aux.setFechaEstreno("31/08/2014");
                aux.setCalificacion("+18");
                aux.setCategoria("Terror");
                dataSource.add(aux);
            }
        }
      */

        return ColeccionPelicula.getPelicula();
    }
}
